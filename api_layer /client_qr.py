# -*- coding: utf-8 -*-
import os
import select
import sys

sys.path.append("..")
from random import random
from mesh_net.nodes_in_mesh import *
import physical_layer.qr_utils as qr_utils

import time
from config_qrnet import ConfigQRNet
from twisted.python.compat import raw_input

config = ConfigQRNet()


# Esta función inicia la interacción con el servidor IRC
# Entrada: None Salida: None
def start_chat():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.connect((config.SERVER_IRC_IP, config.LISTENING_IRC_PORT))
    nick = raw_input("Por favor introduce tu nick > ")
    channel = raw_input("Escriba el canal al que desea ingresar > ")
    print("\n")
    # Enviamos el nick al server
    server.send(nick.encode("ascii"))
    server.send(channel.encode("ascii"))
    # Recibimos la confirmación o denegación del server
    truck = server.recv(1).decode('UTF-8')

    # Mientras sea 0 es decir lo deniege entramos en el loop hasta que reciba 1 es decir lo acepte
    while (truck == "0"):
        nick = raw_input("\nEse nick ya está en uso, por favor introduce otro > ")
        server.send(nick.encode("ascii"))
        truck = server.recv(1).decode('UTF-8')

    while True:
        # Mantiene una lista de posible flujos de entrada
        sockets_list = [sys.stdin, server]

        """Hay dos posibles situaciones de entrada. O el usuario quiere dar una entrada manual para
        enviar a otras personas o el servidor está enviando un mensaje para ser impreso en pantalla.
        Selecciona las devoluciones de sockets_list, la stream que es el lector para la entrada.
        Entonces, por ejemplo si el servidor quiere enviar un mensaje, entonces la condicion if
        se mantendrá verdadera. Si el usuario desea enviar un mensaje, la condición se evaluará como 
        verdadera """

        read_sockets, write_socket, error_socket = select.select(sockets_list, [], [])

        for socks in read_sockets:

            if socks == server:
                message = socks.recv(2048).decode('UTF-8')
                print(message)
            else:
                message = sys.stdin.readline()
                server.send(message.encode("ascii"))
                server.send(channel.encode("ascii"))
                # Borramos la linea que escribimos por estética
                CURSOR_UP = '\033[F'
                ERASE_LINE = '\033[K'
                print(CURSOR_UP + ERASE_LINE)
                sys.stdout.write("<#" + channel + "-" + "Tu>")
                sys.stdout.write(message)
                sys.stdout.flush()
    server.close()


# Función que crea un socket para enviar mensajes
# a través del dispositivo de transmisión
# Entrada: string -> mac_address destino Salida: None
def start_remote_qr(mac_destiny, type_message, message):
    message_to_send = "MAC " + mac_destiny + " " + type_message + " " + message

    socket_qr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print((config.TRANSMISSION_DEVICE_IP, config.TRANSMISSION_PORT))
    socket_qr.connect((config.TRANSMISSION_DEVICE_IP, config.TRANSMISSION_PORT))

    message_to_send = message_to_send.encode('ascii')
    socket_qr.send(message_to_send)

    socket_qr.close()

# Función que levanta la conexión con el servidor DISPOSITIVO DE TRANSMISIÓN
# Recibe como entrada la mac address del CLIENTE que contacta.
def receive_transmission_device(my_mac):
    socket_send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_send.connect((config.NET_MESH_IP, config.INFO_MESH_PORT))
    socket_send.send(my_mac.encode('ascii'))
    info = socket_send.recv(2048).decode('UTF-8')
    socket_send.close()

    info = info.split(' ')

    ip = info[0]
    port = int(info[1])

    socket_device = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_device.bind((ip, port+1))
    socket_device.listen()

    while True:
        conn, address = socket_device.accept()
        reconstruct = Thread(target=reconstruct_message, args=(conn,))
        #reconstruct.daemon = True
        reconstruct.start()
        reconstruct.join()

# Función encargada de reconstruir el mensaje enviado desde el servidor. Lo que 
# hace realmente es concatenar cada uno de los códigos binarios que le llegan.
# Entrada: conn la cual representa la conexión con el servidor permitiendo el recibir
# información 
def reconstruct_message(conn):
    data_list = []

    number = int(conn.recv(2048).decode('UTF-8'))

    for i in range(number):
        data_list.append(conn.recv(2048).decode('UTF-8'))
    conn.close()

    qr_reconstruction(data_list)


# Función que se encarga de llamar la implementación de reconstrucción del mensaje.
# Dependiendo de si es un simple mensaje o un archivo esta función llamará la 
# función correcpondiente.
# Entrada: data_list -> todos los códigos binarios que forman el mensaje 
def qr_reconstruction(data_list):
    constructor = qr_utils.qr_reader()
    type_m = data_list[0].split(",")[1]

    if type_m == 'f':
        print('Recibiste un archivo via QR!!!')
        constructor.reconstruct_file(is_list=True, data=data_list)
    elif type_m == 't':
        print('Recibiste un mensaje de texto via QR!!!')
        constructor.qr_to_text(save_in_file=False, info=data_list[0])


# Función que genera una dirección Mac address para cada nuevo nodo en la red mesh
def macGenerator():
    new_mac = ''
    for m in range(0, 12):
        new_c = random.randint(0, 9)
        new_mac += str(new_c)
    return new_mac


# Conecta con el servidor de registros de la red mesh
# Entrada: int -> puerto Salida: String->mac_address del usuario
def register_mesh(port):
    my_mac = macGenerator()
    socket_mesh = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_mesh.connect((config.NET_MESH_IP, config.REGISTRY_MESH_PORT))

    register = "MAC " + my_mac + " PORT " + port

    socket_mesh.send(register.encode("ascii"))
    received_data = socket_mesh.recv(2048).decode("UTF-8")
    data = received_data.split(' ')

    if data[0] == 'OK/200':
        print("Se integró correctamente el nodo a la red")

    else:
        if data[0] == 'DULPICATEM':
            print("Mac address ya pertenece a la red mesh")

        elif data[0] == 'DULPICATEP':
            print("IP ya presenta este puerto")

        else:
            print("Error al agregar nodo a la red mesh ")

        sys.exit(2)

    return my_mac


# Función que verifica los mensajes enviados por la red mesh para saber si el mensaje
# está dirigido al nodo actual
def verify_frame(conn):
    new_msj = conn.recv(2048).decode('UTF-8')
    new_msj = new_msj.split(' ')

    # Debemos ver si el mensaje es para nuestro nodo actual
    if new_msj[0] == 'MESSAGE':
        print(">> Mensaje nuevo: {0}".format(" ".join(str(x) for x in new_msj[1:])))

    elif new_msj[0] == 'RESEND':
        print("Reenviando mensaje a destinatario real")
        ip_dest = new_msj[1]
        port_dest = int(new_msj[2])
        resend_msj = ""

        if new_msj[3] != 'MESSAGE':
            resend_msj = 'RESEND '

        resend_msj += " ".join(str(x) for x in new_msj[3:])

        socket_resend = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket_resend.connect((ip_dest, port_dest))
        print("\n Mensaje enviado a {0} {1}, {2}".format(ip_dest, port_dest, resend_msj))
        socket_resend.send(resend_msj.encode('ascii'))
        socket_resend.close()

    conn.close()


# Empaqueta el mensaje para ser enviado por la red mesh


def prepare_send_in_mesh(origin, destiny, message):
    socket_mesh = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_mesh.connect((config.NET_MESH_IP, config.RECEIVING_MESH_PORT))

    path_msj = "SRC " + origin + " DST " + destiny
    socket_mesh.send(path_msj.encode('ascii'))
    path = socket_mesh.recv(2048).decode('UTF-8')
    path = path.split(' ')

    # Caso en el que el nodo no se haya registrado en la red
    if path[0] == 'UNREGISTERED':
        print("La mac a la que desea enviar el mensaje no existe")

    elif path[0] == 'PATH':
        message = 'MESSAGE ' + message

        if len(path) > 3:
            message = 'RESEND ' + " ".join(str(x) for x in path[3:]) + " " + message

        socket_resend = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket_resend.connect((path[1], int(path[2])))
        print("\n Mensaje enviado a {0} {1}, {2}".format(path[1], path[2], message))
        socket_resend.send(message.encode('ascii'))
        socket_resend.close()

        print("Enviando mensaje...")

    socket_mesh.close()


# Inicializa todos los preparativos para enviar un mensaje por la red mesh
def send_mesh(mac_origin):
    while True:
        print('Formato de mensaje: SEND <mac_destino> <mensaje a enviar>')
        message_in_mesh = str(input("#>> "))
        message_in_mesh = message_in_mesh.split(' ')
        if message_in_mesh[0] == 'SEND' and len(message_in_mesh) >= 3:

            if len(message_in_mesh[1]) != 12:
                print("La direccion destino esta incorrecta, por favor verificar mac address")

            else:
                mac_dst = message_in_mesh[1]
                sending_msj = " ".join(str(x) for x in message_in_mesh[2:])
                prepare_send_in_mesh(mac_origin, mac_dst, sending_msj)
                break
        else:
            print("Error: Mensaje no posee la estructura adecuada introduzcalo de nuevo\n")


# Es el buzón de entrada del cliente para los mensajes de la red mesh
def receive_in_mesh(my_mac):

    socket_send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_send.connect((config.NET_MESH_IP, config.INFO_MESH_PORT))
    socket_send.send(my_mac.encode('ascii'))
    info = socket_send.recv(2048).decode('UTF-8')
    socket_send.close()

    info = info.split(' ')

    socket_mesh = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_mesh.bind((info[0], int(info[1])))
    socket_mesh.listen()
    while True:
        conn, address = socket_mesh.accept()
        verify = Thread(target=verify_frame, args=(conn,))
        verify.start()
        verify.join()

# Elimina la mac de entrada de la red mesh solicitando al server mesh que lo realice 
def delete_from_mesh(my_mac):
    socket_resend = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_resend.connect((config.NET_MESH_IP, config.DELETE_PORT))
    socket_resend.send(my_mac.encode('ascii'))
    socket_resend.close()


def main():
    # Registro en la red mesh
    print("Bienvenido a Remote-QR")
    print("Escriba un puerto para su conexión en el servidor")
    port = sys.stdin.readline()

    my_mac = register_mesh(port)

    recv_mesh = Thread(target=receive_in_mesh, args=(my_mac,))
    recv_mesh.daemon = True
    recv_mesh.start()

    device = Thread(target=receive_transmission_device, args=(my_mac,))
    device.daemon = True
    device.start()


    while True:
        type = ''
        selection = ''
        os.system('clear')
        print("Tu dirección MAC es: "+ my_mac)
        print("Seleccione la acción a realizar")
        print("1. Crear QR nuevo")
        print("2. Entrar al servidor IRC")
        print("3. Enviar mensaje en la red Mesh")
        print("4. Salir del servicio")
        option = int(sys.stdin.readline())

        if option == 1:

            print("Qué tipo de mensaje desea enviar")
            print("1. Texto")
            print("Cualquier otra cosa. Archivo")
            selection = raw_input()
            if selection == '1':
                type = 't'
                print("Escriba el texto a enviar")

            else:
                type = 'f'
                print("Escriba la ruta del archivo a enviar")

            message = raw_input()
            print("Escriba la mac destino")
            dest_mac = raw_input()
            #message.rstrip('\r\n')
            #dest_mac.rstrip('\r\n')
            start_remote_qr(dest_mac, type, message)
            print("QR creado con éxito")
            time.sleep(1)

        elif option == 2:
            start_chat()


        elif option == 3:
            send_mesh(my_mac)

        elif option == 4:
            print("Nos vemos pronto")
            delete = Thread(target=delete_from_mesh, args=(my_mac,))
            delete.daemon = True
            delete.start()
            sys.exit()

        else:
            print("Debe seleccionar una acción válida")
            time.sleep(1)


if __name__ == "__main__":
    main()
