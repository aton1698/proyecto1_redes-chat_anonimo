# -*- coding: utf-8 -*-
# Server chat con socket en python
import socket
import sys
sys.path.append("..")
from _thread import start_new_thread
from config_qrnet import ConfigQRNet

# Aquí guardaremos los clientes que se conecten
list_of_clients = []
nicks = {"admin": "admin"}
config = ConfigQRNet()

def clientthread(conn, addr):
    # Le enviamos un mensaje al nuevo cliente conectado
    conn.send(("Bienvenido al chat " + nicks[conn] + "!!!").encode("ascii"))

    while True:
        try:
            message = conn.recv(2048).decode('UTF-8')
            channel = conn.recv(2048).decode('UTF-8')

            if message:
                # Imprime direccion y mensaje del usuario
                if message == '/EXIT\n':
                    print('Adios ' + nicks[conn])
                    remove([conn, channel])
                    conn.close()
                else:
                    print("<#" + channel + "-" + addr[0] + " " + nicks[conn] + " > " + message)

                    # Llamamos a la funcion broadcast que pondré ahora para enviar mensaje a todos
                    message_to_send = "<#" + channel + "-" + nicks[conn] + "> " + message
                    broadcast(message_to_send, conn, channel)
            else:
                """El mensaje puede no tener contenido si la conexion
                está rota, en este caso eliminamos la conexión """
                # La función remove la escribiré ahora
                remove([conn, channel])
        except:
            continue


def broadcast(message, connection, channel):
    for clients in list_of_clients:
        if clients[0] != connection:
            try:
                if clients[1] == channel:
                    clients[0].send(message.encode("ascii"))
                else:
                    continue
            except:
                clients[0].close()
                # Si el enlace esta roto eliminamos el cliente
                remove(clients)


def remove(connection):
    if connection in list_of_clients:
        list_of_clients.remove(connection)




def power_up_chat():
    print("Welcome to the chat " + config.SERVER_IRC_IP + "!!!")
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    server.bind((config.SERVER_IRC_IP, config.LISTENING_IRC_PORT))
    server.listen(100)

    while True:
        """Acepta una solicitud de conexión y almacena dos parámetros, conn que un objeto socket
        y addr que contiene la dirección IP del cliente """
        conn, addr = server.accept()
        # El primer mensaje que recibamos será el nick, lo veremos en el cliente cuando termine el server
        nick = conn.recv(2048).decode('UTF-8')
        channel = conn.recv(2048).decode('UTF-8')
        """Metemos al cliente en la lista de clientes """
        list_of_clients.append([conn, channel])

        """Comprobamos que el nick no esta en nuestro diccionario de nicks para que no se repita """
        t = True
        i = 0
        for i in nicks:
            if nicks[i] == nick:
                t = True
            else:
                t = False
        # Si esta disponible lo añadimos
        if t == False:
            nicks[conn] = nick
            # Enviamos una confirmación al cliente
            conn.send("1".encode("ascii"))
        else:
            # Si no esta disponible le enviamos una denegación al server y creamos un bucle hasta que este disponible
            while t:
                conn.send("0".encode("ascii"))
                nick = conn.recv(2048).decode('UTF-8')
                if nicks[i] == nick:
                    t = True
                else:
                    t = False
                    nicks[conn] = nick
                    conn.send("1".encode("ascii"))
        # Ahora imrimos la direccion y el nick del usuario que se acaba de conectar
        print(addr[0] + " " + nick + " conectado")

        # Creamos un proceso individual para el cliente que se conecta
        start_new_thread(clientthread, (conn, addr))
    conn.close()
    server.close()

    return 0

if __name__ == "__main__":
    if len(sys.argv) != 1:
        print("Error: Debe introducir server_qr.py")
        exit()

    try:

        power_up_chat()
    except KeyboardInterrupt as e:
        print('[+] Bye!')



