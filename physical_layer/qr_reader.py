import sys
import time

from twisted.python.compat import raw_input

sys.path.append("..")
from socket import *
from qr_utils import *
from mesh_net.nodes_in_mesh import *

def main():
    reader = qr_reader()

    print('Cómo desea leer los QR? \n')
    print('1. Mediante camara\n')
    print('2. Mediante una carpeta\n')

    option = raw_input()

    qr_list = []

    if option == '1':
        #Qr_list es una lista con los QR escaneados
        qr_list = reader.scan()

    elif option == '2':
        print('Por favor ingrese la ruta de la carpeta donde se encuentran los QR \n')
        qr_path = raw_input()

        print('Ingrese la cantidad de QR a leer \n')
        qr_amount = int(raw_input())

        qr_list = reader.read_from_folder(qr_amount, qr_path)
    else:
        print("Error al elegir en el menu")
        exit()

    socket_send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_send.connect((config.NET_MESH_IP, config.INFO_MESH_PORT))
    socket_send.send(qr_list[0]['mac'].encode('ascii'))
    info = socket_send.recv(2048).decode('UTF-8')
    socket_send.close()

    info = info.split(' ')

    ip_dest = info[0]
    port_dest = int(info[1])+1

    s_send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s_send.connect((ip_dest, port_dest))

    s_send.send(str(len(qr_list)).encode('ascii'))

    frame = Frame()
    for qr in qr_list:
        time.sleep(0.1)
        d = frame.new_frame(qr['mac'], qr['type'], qr['message'])

        s_send.send(d.encode('ascii'))
    print('Se ha enviado con exito')
    s_send.close()

if __name__ == "__main__":

    main()


